<?php

namespace cebe\markdown\inline;

trait FootnoteTrait
{
	private $footnoteReferences = [];

	/**
	 * Parses a footnote reference indicated by `[^`.
	 * @marker [^
	 */
	protected function parseFootnote($markdown)
	{
		if (preg_match('/\[\^(\w?)\]/s', $markdown, $matches)) {
			$cssId = htmlentities($matches[1]);
			$text = $this->parseInline($matches[1]);
			$this->footnoteReferences[$cssId] = $text;

			return [ [
					'footnote',
					'text' => $text,
					'cssId' => $cssId,
			], strlen($matches[0])];
		}
	}

	protected function renderFootnote($block)
	{
		return '<sup id="fnref-'.$block['cssId'].'"><a href="#fn-'.$block['cssId'].'" class="footnote-ref">'.
				$this->renderAbsy($block['text']).
			'</a></sup>';
	}

	abstract protected function parseInline($text);
	abstract protected function renderAbsy($text);
}
