<?php
/**
 * @copyright Copyright (c) 2014 Carsten Brandt
 * @license https://github.com/cebe/markdown/blob/master/LICENSE
 * @link https://github.com/cebe/markdown#readme
 */

namespace cebe\markdown\block;

/**
 * Adds the list blocks
 */
trait FootnoteTrait
{
	/**
	 * Parses a block indicated by `[^`.
	 * @param string $line
	 * @return bool
	 */
	protected function identifyFootnote($line)
	{
		return ($line[0] === '[' && strncmp($line, '[^', 2) === 0);
	}

	/**
	 * Consume lines for an unordered list
	 * @param string $lines
	 * @param string $current
	 * @return array
	 */
	protected function consumeFootnote($lines, $current)
	{
		// consume until newline
		$type = 'dl';
		$block = [
			'footnotes',
			'list' => $type,
			'items' => [],
		];
		$item = 0;
		$indent = '';
		$leadSpace = 3;
		$len = 0;
		$lastLineEmpty = false;
		// track the indentation of list markers, if indented more than previous element
		// a list marker is considered to be long to a lower level
		for ($i = $current, $count = count($lines); $i < $count; $i++) {
			$line = $lines[$i];
			// match list marker on the beginning of the line
			$pattern = '/^( {0,'.$leadSpace.'})\[\^(\w+)\]:[ \t]+/';
			if (preg_match($pattern, $line, $matches)) {
				if (($len = substr_count($matches[0], "\t")) > 0) {
					$indent = str_repeat("\t", $len);
					$line = substr($line, strlen($matches[0]));
				} else {
					$len = strlen($matches[0]);
					$indent = str_repeat(' ', $len);
					$line = substr($line, $len);
					$reference = $matches[2];
				}
				if ($i === $current) {
					$leadSpace = strlen($matches[1]) + 1;
				}

				$block['items'][++$item][] = $line;
				$block['references'][$item] = $reference;
				$block['lazyItems'][$item] = $lastLineEmpty;
				$lastLineEmpty = false;
			} elseif (ltrim($line) === '') {
				// line is empty, may be a lazy list
				$lastLineEmpty = true;

				// two empty lines will end the list
				if (!isset($lines[$i + 1][0])) {
					break;

				// next item is the continuation of this list -> lazy list
				} elseif (preg_match($pattern, $lines[$i + 1])) {
					$block['items'][$item][] = $line;
					$block['lazyItems'][$item] = true;

				// next item is indented as much as this list -> lazy list if it is not a reference
				} elseif (strncmp($lines[$i + 1], $indent, $len) === 0 || !empty($lines[$i + 1]) && $lines[$i + 1][0] == "\t") {
					$block['items'][$item][] = $line;
					$nextLine = $lines[$i + 1][0] === "\t" ? substr($lines[$i + 1], 1) : substr($lines[$i + 1], $len);
					$block['lazyItems'][$item] = !method_exists($this, 'identifyReference') || !$this->identifyReference($nextLine);

				// everything else ends the list
				} else {
					break;
				}
			} else {
				if ($line[0] === "\t") {
					$line = substr($line, 1);
				} elseif (strncmp($line, $indent, $len) === 0) {
					$line = substr($line, $len);
				}
				$block['items'][$item][] = $line;
				$lastLineEmpty = false;
			}
		}

		foreach($block['items'] as $itemId => $itemLines) {
			$content = [];
			if (!empty($itemLines)) {
				$content = array_merge($content, $this->parseBlocks($itemLines));
			}
			$block['items'][$itemId] = $content;
//			if (!isset($block['references'][$itemId])) {
//				$block['references'][$itemId] = null;
//			}
		}

		return [$block, $i];
	}

	/**
	 * Renders a list
	 */
	protected function renderFootnotes($block)
	{
		$type = $block['list'];

		$output = '<hr/>' . "\n" . "<$type>\n";

		foreach ($block['items'] as $item => $itemLines) {
			$reference = $block['references'][$item];
			$itemLines[0]['content'][] = [
				'link',
				'url' => '#fnref-' . $reference,
				'text' => $this->parseInline('&#8617;'),
			];
			$output .= '<dd id="fn-'.$reference.'">' . $this->renderAbsy($itemLines) . "</dd>\n";
		}
		return $output . "</$type>\n";
	}

	abstract protected function parseBlocks($lines);
	abstract protected function renderAbsy($absy);
}
