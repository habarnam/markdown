Test for a first footnote[^1]. My name is Marius[^2] and I am working on an implementation of footnotes[^3] for Mardown Extra[^4].

[^1]: The explanation for the first footnote.
[^2]: Marius O.<marius@habarnam.ro>

[^3]: See [reference](https://michelf.ca/projects/php-markdown/extra/#footnotes)

[^4]: Markdown Extra is an extension to PHP Markdown implementing some features currently not available with the plain Markdown syntax.
  https://michelf.ca/projects/php-markdown/extra/
